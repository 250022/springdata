package com.example.springdata;

import com.example.springdata.MODEL.*;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {

    private ProductRepo pRepo;
    private OrderRepo oRepo;
    private CustomerRepo cRepo;

    public DbMockData(ProductRepo pRepo, OrderRepo oRepo, CustomerRepo cRepo) {
        this.pRepo = pRepo;
        this.oRepo = oRepo;
        this.cRepo = cRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill(){
    /*    Product product = new Product("Długopis",2.99f,true);
        Product product1 = new Product("Ołówek", 1.99f, true);

        Customer customer = new Customer("Anna Kowalska","Leszno");
        Set<Product>products = new HashSet<>(){
            {
                add(product);
                add(product1);
            }
        };

        CustomerOrder order = new CustomerOrder(customer,products,LocalDateTime.now(),"send");
        pRepo.save(product);
        pRepo.save(product1);
        cRepo.save(customer);
        oRepo.save(order); */
    }


    //spring.datasource.url=jdbc:h2:file./bazadanych1
}
